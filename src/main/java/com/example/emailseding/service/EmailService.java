package com.example.emailseding.service;

import com.example.emailseding.model.EmailDetails;

public interface EmailService {
    // To send a simple email
    String sendSimpleMail(EmailDetails details);

}
