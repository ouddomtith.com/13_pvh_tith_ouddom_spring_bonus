package com.example.emailseding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmailSedingApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmailSedingApplication.class, args);
    }

}
